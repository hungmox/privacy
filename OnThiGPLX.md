LƯU Ý: XIN HÃY ĐỌC KĨ CÁC ĐIỀU KHOẢN, CHÍNH SÁCH BẢO MẬT VÀ THỎA THUẬN NGƯỜI DÙNG DƯỚI ĐÂY. BẠN CHỈ SỬ DỤNG ỨNG DỤNG KHI ĐÃ HIỂU CÁC CHÍNH SÁCH BẢO MẬT CỦA CHÚNG TÔI CŨNG NHƯ ĐỒNG Ý VỚI TẤT CẢ CÁC ĐIỀU KHOẢN TRONG THỎA THUẬN NÀY

I. CHÚNG TÔI KHÔNG THU THẬP BẤT CỨ DỮ LIỆU GÌ TỪ NGƯỜI DÙNG
CHúng tôi không thu thập bất kì dữ liệu nào từ người dùng. Chúng tôi sử dụng máy ảnh, micro của bạn để phực vụ trực tiếp cho các chức năng của ứng dụng. Chúng tôi không hề lưu lại cũng như gửi dữ liệu này tới bất kì đâu.

II. Ứng dụng cần được cấp một số quyền nhằm:
- Notification: Để thông báo nhắc nhở người dùng học các bài học khi đến giờ học nhất định
- Máy ảnh: Cho phép ứng dụng truy cập camera để chụp ảnh đại diện
- Thư viện ảnh: Cho phép ứng dụng truy cập thư viện để chọn ảnh đại diện

III. Truy cập trang từ bên thứ 3
Trong trường hợp ứng dụng hoặc dịch vụ của chúng tôi truy cập vào các trang từ bên thứ 3. Chúng tôi có thể cung cấp đường link tới các trang web này như là một dịch vụ cho người dùng. Những trang này có thể bao gồm các thông tin và quảng cáo từ các công ty khác. Khi bạn click và các link này. Trong trường hợp bạn không muốn truy cập các trang này bạn có thể rời khỏi ứng dụng của chúng tôi. Chúng tôi không thu thập dữ liệu và không chịu trách nhiệm về chính sách bảo mật của các website bên thứ 3.

IV. Bảo mật
Chúng tôi không thu thập bất cứ dữ liệu nào của bạn do vậy cũng không cần đến việc bảo mật dữ liệu này. Ứng dụng không can thiệp và hoạt dộng thông thường của ứng dụng khác cũng như ảnh hưởng tới thiết bị của bạn.

V. Trách nhiệm
Chúng tôi không chịu trách nhiệm về độ chính xác của nội dung cung cấp trong ứng dụng. Chúng tôi cũng không chịu trách nhiệm cho những ảnh hưởng của việc sử dụng kiến thức trong ứng dụng tới bạn và cuộc sống của bạn. Chúng tôi cũng không chịu trách nhiệm về sự mất mát dữ liệu mà bạn thêm vào ứng dụng nếu điều đó xảy ra.

VI. Liên hệ
Nếu bạn có thắc mắc gì về chính sách bảo mật của ứng dụng vui lòng liên hệ với chúng tôi qua email vudinhhung.72@gmail.com
